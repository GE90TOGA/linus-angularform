import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../model/user';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  titles = ['Mr.', 'Miss.', 'Mrs.'];
  countries = ['Australia', 'China', 'USA'];
  user: User;
  submitted = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.user = new User(UserService.getUserID(), 'Mr.', '', '', 'Australia', '', '')
  }

  onSubmit() {
    this.submitted = true;
    console.log(JSON.stringify(this.user));
  }

  // TODO: Remove this when we're done
  // get diagnostic() { return JSON.stringify(this.user); }
}
