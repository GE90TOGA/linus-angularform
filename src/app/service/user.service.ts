import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  static totalUserNum = 0;

  constructor() { }

  static getUserID(): number {
    return ++this.totalUserNum;
  }

}
