import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './page/register/register.component'

const routes: Routes = [
  { path: '', redirectTo: '/registe', pathMatch: 'full' },
  { path: 'registe', component: RegisterComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
